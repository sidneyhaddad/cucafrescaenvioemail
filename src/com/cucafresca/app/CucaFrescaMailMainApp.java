package com.cucafresca.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;

import com.cucafresca.manager.HoleriteEmailManager;
import com.cucafresca.manager.HoleriteManager;
import com.cucafresca.util.MailUtil;
import com.cucafrescal.on.Holerite;

public class CucaFrescaMailMainApp {
	public static boolean DEBUG = false;
	public static String FILE_APP_PROPERTIES = "conf/app.properties";
	public static Properties APP_PROPERTIES = null; 
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		
		
		loadProperties();
		Logger.getGlobal().info("App properties loaded: " + FILE_APP_PROPERTIES);
		DEBUG = "1".equals(APP_PROPERTIES.getProperty("debug"));
		
		Logger.getGlobal().info("\t" + APP_PROPERTIES);
		
		String folderToSendStr = APP_PROPERTIES.getProperty("pasta.paraLer");
		
		HoleriteManager holeriteManager = new HoleriteManager();
		
		Logger.getGlobal().info("Reading folder " + folderToSendStr);
		File toSendFolder = new File(folderToSendStr);
		Logger.getGlobal().info("\tFolder ready " + toSendFolder.getAbsolutePath());
		
		
		for (File toSendFile : toSendFolder.listFiles()) {
			System.out.println("Lendo arquivo " + toSendFile.getAbsolutePath());
			Logger.getGlobal().info("\tValidating file: " + toSendFile.getAbsolutePath());
			if (holeriteManager.validadeFile(toSendFile)) {
				Logger.getGlobal().info("\t\tFile validated");
				Logger.getGlobal().info("\tParsing file");
				List<Holerite> holeriteList = holeriteManager.readHolerite(toSendFile);
				System.out.println("\t\tNúmero de Holerites encontrado: " + holeriteList.size());
				File fileSaved = null;
				for (Holerite holerite : holeriteList) {
					fileSaved = holeriteManager.saveHolerite(holerite, toSendFile.getName());
				}
				if (fileSaved != null) {
					Logger.getGlobal().info("\t\tHolerites gravados na seguinte pasta: \"" + fileSaved.getParentFile().getAbsolutePath() + "\"");
				}
			}
		}

		System.out.println("Deseja enviar por email os arquivos da pasta \"" + APP_PROPERTIES.getProperty("pasta.paraEnviar") + "\"?");
		System.out.print("Digite \"ok\" para continuar: ");
		if (!new Scanner(System.in).nextLine().equals("ok"))
			System.exit(0);
		
		HoleriteEmailManager holeriteEmailManager = new HoleriteEmailManager();
		holeriteEmailManager.sendHoleriteFiles();
	}


	public static void loadProperties() throws IOException,
			FileNotFoundException {
		APP_PROPERTIES = new Properties();
		APP_PROPERTIES.load(new FileInputStream(new File(FILE_APP_PROPERTIES)));
		
		MailUtil.MAIL_PROPERTIES = APP_PROPERTIES;
	}
}
