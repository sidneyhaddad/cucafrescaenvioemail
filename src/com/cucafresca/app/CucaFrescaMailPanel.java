package com.cucafresca.app;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTree;
import javax.swing.JTextPane;

import com.cucafresca.manager.HoleriteEmailManager;
import com.cucafresca.manager.HoleriteManager;
import com.cucafresca.ui.HoleriteCellRenderer;
import com.cucafresca.ui.InputPropertyJPanel;
import com.cucafresca.ui.PropertyJPanel;
import com.cucafrescal.on.Empresa;
import com.cucafrescal.on.Holerite;

import javax.swing.JTextArea;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JProgressBar;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.SwingConstants;

import java.util.Properties;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Font;

public class CucaFrescaMailPanel extends JPanel {
	private JTextArea txtrLogDeEventos;
	private JTree jTree;
	private JEditorPane txtHtmlRendered;
	private JLabel lblTitle;
	private DefaultTreeModel model;
	private JTextField txtEmail;
	private JButton btnEnviarPorEmail;
	private JProgressBar progressBar;
	private JPanel panelNodeSelected;
	private JSplitPane splitPane;
	private JPanel panelEmail;
	private PropertyJPanel propertyJPanel;
	private JButton btnEnviarTodos;
	
	
	/**
	 * Create the panel.
	 */
	public CucaFrescaMailPanel() {
		System.out.println("Versão 2016-10-24 10:30");
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.25);

		panel_1.add(splitPane, BorderLayout.CENTER);
		splitPane.setOneTouchExpandable(true);
		

		DefaultMutableTreeNode jTreeNode = new DefaultMutableTreeNode("Arquivos");
		jTree = new JTree(jTreeNode);
		jTree.setCellRenderer(new HoleriteCellRenderer());
		JScrollPane jspTree = new JScrollPane(jTree);
		jTree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				try {
					actionNodeSelected();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

		});
		splitPane.setLeftComponent(jspTree);
		
		JPanel panel_7 = new JPanel();
		splitPane.setRightComponent(panel_7);
		splitPane.setDividerLocation(0.25);
		
		
		
		panelEmail = new JPanel();
		
		panelEmail.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		lblTitle = new JLabel("Lista de E-mails");
		lblTitle.setHorizontalAlignment(SwingConstants.LEFT);
		lblTitle.setVerticalAlignment(SwingConstants.TOP);
		panel.add(lblTitle);
		panelEmail.add(panel, BorderLayout.NORTH);
		

		
		JPanel panel_6 = new JPanel();
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    JDialog.setDefaultLookAndFeelDecorated(true);
			    int response = JOptionPane.showConfirmDialog(null, "Deseja salvar os emails?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			    
				if (response == JOptionPane.YES_OPTION) {
					actionSaveEmail();
			    }
			}
		});		
		panel_6.add(btnSalvar);

		panelEmail.add(panel_6, BorderLayout.SOUTH);

		propertyJPanel = new PropertyJPanel((Properties) null);
		
		panelEmail.add(propertyJPanel, BorderLayout.CENTER);
		
		
		
		model = (DefaultTreeModel) jTree.getModel();
		
		panelNodeSelected = new JPanel();
		panelNodeSelected.setLayout(new BorderLayout(0, 0));
		
		txtHtmlRendered = new JEditorPane();
		txtHtmlRendered.setContentType("text/html");
		JScrollPane jsp = new JScrollPane(txtHtmlRendered);
		panelNodeSelected.add(jsp, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		jsp.setColumnHeaderView(panel_2);
		
		txtEmail = new JTextField();
		txtEmail.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				btnEnviarPorEmail.setEnabled(true);
			}
		});
		FlowLayout fl_panel_2 = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_2.setLayout(fl_panel_2);
		panel_2.add(txtEmail);
		txtEmail.setColumns(20);
		
		btnEnviarPorEmail = new JButton("Enviar por e-mail");
		btnEnviarPorEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    JDialog.setDefaultLookAndFeelDecorated(true);
			    int response = JOptionPane.showConfirmDialog(null, "Deseja enviar email para " + txtEmail.getText(), "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			    
				if (response == JOptionPane.YES_OPTION) {
					actionSendMailSelected();
			    }
			}

		});
		panel_2.add(btnEnviarPorEmail);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.NORTH);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_4.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_3.add(panel_4, BorderLayout.WEST);
		
		btnEnviarTodos = new JButton("Enviar Todos");
		panel_4.add(btnEnviarTodos);
		
		JButton btnReenviar = new JButton("Reenviar");
		btnReenviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    int response = JOptionPane.showConfirmDialog(null, "Deseja re-enviar emais que falharam?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			    
				if (response == JOptionPane.YES_OPTION) {
					Thread t1 = new Thread(new Runnable() {
					     public void run() {
					          actionSendMailFalhas();
					     }
					});  
					t1.start();
				}				
			}

		});
		panel_4.add(btnReenviar);
		
		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5, BorderLayout.EAST);
		
		progressBar = new JProgressBar();
		progressBar.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		progressBar.setPreferredSize(new Dimension(200, 29));
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		panel_5.add(progressBar);
		btnEnviarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    int response = JOptionPane.showConfirmDialog(null, "Deseja enviar email para todos resgistros?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			    
				if (response == JOptionPane.YES_OPTION) {
					Thread t1 = new Thread(new Runnable() {
					     public void run() {
					          actionSendMailTodos();
					     }
					});  
					t1.start();
				}				
			}

		});
		
		try {
			initData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		restoreDefaults();

	}
    private void restoreDefaults() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                splitPane.setDividerLocation(splitPane.getSize().width /4);
                splitPane.getRightComponent().revalidate();
                splitPane.getRightComponent().repaint();
            }
        });
    }
	
	private void actionSendMailSelected() {
		try {
			progressBar.setIndeterminate(true);
			progressBar.setVisible(true);
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
			Holerite holerite = (Holerite) node.getUserObject();
			if (!new File(holerite.getFileName()).exists()) {
			    JOptionPane.showMessageDialog(null, "Esse holerite ja foi enviado!");
				return;
			}
			holerite.setEmail(txtEmail.getText());
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						HoleriteEmailManager.sendHolerite(holerite);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					progressBar.setVisible(false);
				}
			});
			t.start();
			
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void actionSendMailFalhas() {
		actionSendMail(false);
	}
	private void actionSendMailTodos() {
		actionSendMail(true);
		btnEnviarTodos.setEnabled(false);
		
	}
	private void actionSendMail(boolean todos) {
		// Contando quantos elementos na tree
		Enumeration<DefaultMutableTreeNode> empresaEnum = ((DefaultMutableTreeNode)model.getRoot()).children();
		int totalNodes = 0;
		while (empresaEnum.hasMoreElements()) {
			DefaultMutableTreeNode empresaNode = empresaEnum.nextElement();
			Enumeration<DefaultMutableTreeNode> holeriteEnum = empresaNode.children();
			while(holeriteEnum.hasMoreElements()) {
				Holerite holerite = (Holerite) holeriteEnum.nextElement().getUserObject();
				if (todos || holerite.isEnviado() == null || !holerite.isEnviado().booleanValue())
					totalNodes += 1;
			}
			
		}
		
		
		// Setting progressBar
		progressBar.setMinimum(0);
		progressBar.setMaximum(totalNodes);
		progressBar.setIndeterminate(false);
		progressBar.setVisible(true);

		// Sending Email
		empresaEnum = ((DefaultMutableTreeNode)model.getRoot()).children();
		int countProgressBar = 0;
		while (empresaEnum.hasMoreElements()) {
			DefaultMutableTreeNode empresaNode = empresaEnum.nextElement();
			String nomeEmpresa = empresaNode.getUserObject().toString();
			progressBar.setString(nomeEmpresa + " - " + (int)(progressBar.getPercentComplete()*100) +  "%"); 
			
			Enumeration<DefaultMutableTreeNode> holeriteEnum = empresaNode.children();
			while (holeriteEnum.hasMoreElements()) {
				DefaultMutableTreeNode holeriteNode = holeriteEnum.nextElement();
				Boolean enviado = null;
				Holerite holerite = (Holerite) holeriteNode.getUserObject();
				try {

					if ((holerite.isEnviado() == null || holerite.isEnviado().booleanValue() == false )&& holerite.getEmail() != null) {
						enviado = HoleriteEmailManager.sendHolerite(holerite);
					}
					
					if (todos || holerite.isEnviado() == null || !holerite.isEnviado().booleanValue()) {
						countProgressBar += 1;
						progressBar.setValue(countProgressBar);
						progressBar.setString(nomeEmpresa + " - " + (int)(progressBar.getPercentComplete()*100) +  "%"); 
						
					}
				} catch (Exception e1) {
					enviado = false;
					e1.printStackTrace();
					Logger.getGlobal().log(Level.SEVERE, "Erro ao enviar email", e1);
				}
				if (enviado != null)
					holerite.setEnviado(new Boolean(enviado));
				model.reload(holeriteNode);
			}
			model.reload(empresaNode);
			
		}
		
		progressBar.setVisible(false);
	}
	private void actionNodeSelected() throws Exception {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
		if (node.getUserObject() instanceof Holerite) {
			Holerite holerite = (Holerite) node.getUserObject();
			
			txtHtmlRendered.setText(holerite.getHtml());
			txtHtmlRendered.setCaretPosition(0);
			
			txtEmail.setText(holerite.getEmail());	
			txtEmail.setCaretPosition(0);

			splitPane.setRightComponent(panelNodeSelected);
			
		} else if (node.getUserObject() instanceof Empresa) {
			Empresa empresa = (Empresa) node.getUserObject();
			Properties propEmpresa = HoleriteEmailManager.getEmailProperties(empresa.getCodigo());
			Properties propPanel = new Properties();
			Enumeration enumHolerite = node.children();
			while (enumHolerite.hasMoreElements()) {
				Holerite holerite = (Holerite) ((DefaultMutableTreeNode) enumHolerite.nextElement()).getUserObject();
				String emailKey = holerite.getEmailKey();
				propPanel.put(emailKey, propEmpresa.get(emailKey));
			}
			
			propertyJPanel.setProperties(propPanel);
			lblTitle.setText("Lista de emails " + empresa.getNome());
			splitPane.setRightComponent(panelEmail);
			
		} else if (node.getUserObject() instanceof String) {
			lblTitle.setText("Lista de emails todas empresas");
			propertyJPanel.setProperties(HoleriteEmailManager.getEmailProperties());
			splitPane.setRightComponent(panelEmail);
		}
		restoreDefaults();
	}

	private void actionSaveEmail() {
		progressBar.setIndeterminate(true);
		progressBar.setVisible(true);
		progressBar.setString("Saving...");
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				Properties propToSave = propertyJPanel.getProperties();
				Logger.getGlobal().info("Saving properties: " + propToSave);
				
				try {
					HoleriteEmailManager.saveEmailPropertie(propToSave);
					
					Enumeration<DefaultMutableTreeNode> empresaEnum = ((DefaultMutableTreeNode)model.getRoot()).children();
					int totalNodes = 0;
					while (empresaEnum.hasMoreElements()) {
						DefaultMutableTreeNode empresaNode = empresaEnum.nextElement();
						Enumeration<DefaultMutableTreeNode> holeriteEnum = empresaNode.children();
						while(holeriteEnum.hasMoreElements()) {
							Holerite holerite = (Holerite) holeriteEnum.nextElement().getUserObject();
							HoleriteManager.updateEmail(holerite);
						}
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				progressBar.setVisible(false);
				
			}
		});
		t.start();

		
		
	}	
	public void initData() throws Exception {
		CucaFrescaMailMainApp.loadProperties();

		Logger.getGlobal().info("App properties loaded: " + CucaFrescaMailMainApp.FILE_APP_PROPERTIES);
		CucaFrescaMailMainApp.DEBUG = "1".equals(CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("debug"));
		
		Logger.getGlobal().info("\t" + CucaFrescaMailMainApp.APP_PROPERTIES);
		
		String folderToSendStr = CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("pasta.paraLer");
		
		HoleriteManager holeriteManager = new HoleriteManager();
		
		Logger.getGlobal().info("Reading folder " + folderToSendStr);
		File toSendFolder = new File(folderToSendStr);
		Logger.getGlobal().info("\tFolder ready " + toSendFolder.getAbsolutePath());
		
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) jTree.getModel().getRoot();
		for (File toSendFile : toSendFolder.listFiles()) {
			Logger.getGlobal().info("Lendo arquivo " + toSendFile.getAbsolutePath());
			Logger.getGlobal().info("\tValidating file: " + toSendFile.getAbsolutePath());
			
			if (holeriteManager.validadeFile(toSendFile)) {
				Empresa empresa = new Empresa(toSendFile.getName());
				DefaultMutableTreeNode fileNode = new DefaultMutableTreeNode(empresa);
				rootNode.add(fileNode);
				
				Logger.getGlobal().info("\t\tFile validated");
				Logger.getGlobal().info("\tParsing file");
				List<Holerite> holeriteList = holeriteManager.readHolerite(toSendFile);
				Logger.getGlobal().info("\t\tNúmero de Holerites encontrado: " + holeriteList.size());
				File fileSaved = null;
				for (Holerite holerite : holeriteList) {
					if (empresa.getNome() == null)
						empresa.setNome(holerite.getCabecalho().getNomeEmpresa());
					
					fileSaved = holeriteManager.saveHolerite(holerite, toSendFile.getName());

					DefaultMutableTreeNode holeriteNode = new DefaultMutableTreeNode(holerite.getCabecalho());
					holeriteNode.setUserObject(holerite);
					fileNode.add(holeriteNode);
					
				}
				if (fileSaved != null) {
					Logger.getGlobal().info("\t\tHolerites gravados na seguinte pasta: \"" + fileSaved.getParentFile().getAbsolutePath() + "\"");
				}
			}
		}	
		model.reload();
				
	}
	
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame jFrame = new JFrame();
					jFrame.setTitle("Cuca Fresca Mail");
					jFrame.setSize(955, 665);
					jFrame.setLocationRelativeTo(null);
					jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					jFrame.getContentPane().add(new CucaFrescaMailPanel());
					jFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}	
}
