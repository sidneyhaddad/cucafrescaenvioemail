package com.cucafresca.ui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.cucafresca.app.CucaFrescaMailPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.BorderLayout;
import java.awt.GridLayout;

public class PropertyJPanel extends JPanel {
	private Properties properties;
	private JPanel jPanel;

	/**
	 * Create the panel.
	 */
	public PropertyJPanel(Properties properties) {
		setProperties(properties);

	}
	public void setProperties (Properties properties) {
		this.removeAll();
		setLayout(new BorderLayout(0, 0));
		jPanel = new JPanel();
		jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
		this.properties = properties;
		
		int height = 0;
		int posX = -30;
		if (properties != null) {
			Enumeration<Object> enumKey = properties.keys();
			while (enumKey.hasMoreElements()) {
				posX = posX + 30;
				String key = enumKey.nextElement().toString();
				InputPropertyJPanel inputPropertyJPanel = new InputPropertyJPanel(key, properties.getProperty(key));
				height = inputPropertyJPanel.getHeight();
				inputPropertyJPanel.setBounds(0, posX, inputPropertyJPanel.getWidth(), height);
				jPanel.add(inputPropertyJPanel);
			}
		}
		jPanel.setBounds(0, 0, posX+30, height);
		
		
		JScrollPane scrollPane = new JScrollPane(jPanel);
		add(scrollPane, BorderLayout.CENTER);
		
		
		this.revalidate();
		this.repaint();
	}
	public void saveProperties() {
		this.properties = getProperties();
	}
	public Properties getProperties() {
		Properties propOut = new Properties();
		for (Component component : jPanel.getComponents()) {
			if (component instanceof InputPropertyJPanel) {
				InputPropertyJPanel inputPropertyJPanel = (InputPropertyJPanel) component;
				String key = inputPropertyJPanel.getKey();
				String value = inputPropertyJPanel.getValue();
				if (value != null && !value.isEmpty())
					propOut.put(key, value);
				
			}
		}
		return propOut;
		
	}
	public static void main(String[] args) {
		Properties props = new Properties();
		props.setProperty("key1", "value1");
		props.setProperty("key2", "value2");
		props.setProperty("key3", "value1");
		props.setProperty("key4", "value2");
		props.setProperty("key5", "value1");
		props.setProperty("key6", "value2");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame jFrame = new JFrame();
					jFrame.setTitle("Cuca Fresca");
					jFrame.setSize(955, 665);
					jFrame.setLocationRelativeTo(null);
					jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					jFrame.getContentPane().add(new PropertyJPanel(props));
					jFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
