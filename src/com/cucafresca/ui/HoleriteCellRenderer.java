package com.cucafresca.ui;

import java.awt.Color;
import java.awt.Component;
import java.util.Enumeration;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.cucafrescal.on.Empresa;
import com.cucafrescal.on.Holerite;

public class HoleriteCellRenderer extends DefaultTreeCellRenderer {
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        DefaultMutableTreeNode node = ((DefaultMutableTreeNode) value);
        if (node.getUserObject() instanceof Holerite) {
            // Assuming you have a tree of Strings
            Holerite holerite = (Holerite) node.getUserObject();

            if (holerite.isEnviado() != null) {
            	if (holerite.isEnviado().booleanValue()) {
                	setForeground(Color.GREEN);
                } else {
                	setForeground(Color.RED);
                }
            	
            }
            
        } else if (node.getUserObject() instanceof Empresa) {

            // Se tiver 1 vermelho, o parent fica vermelho
            Enumeration<DefaultMutableTreeNode> enumNode = node.children();
            Boolean hasNaoEnviado = null;
            while (enumNode.hasMoreElements()) {
            	Holerite holeriteBrother = (Holerite) enumNode.nextElement().getUserObject();
            	if (holeriteBrother.isEnviado() != null) {
            		hasNaoEnviado = new Boolean(false);
            		if (!holeriteBrother.isEnviado().booleanValue()) {
	            		hasNaoEnviado = new Boolean(true);
	            		break;
            		}
            	}
            }
            if (hasNaoEnviado != null)
            	setForeground(hasNaoEnviado.booleanValue() ? Color.RED : Color.GREEN);
        }


        return this;

	}
}
