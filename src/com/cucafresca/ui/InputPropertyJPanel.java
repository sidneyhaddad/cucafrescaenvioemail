package com.cucafresca.ui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Rectangle;
import java.awt.Dimension;

public class InputPropertyJPanel extends JPanel {
	private JTextField txtValue;
	private JLabel lblkey;

	public InputPropertyJPanel(String key, String value) {
		this();
		setProperty(key, value);
	}
	/**
	 * Create the panel.
	 */
	public InputPropertyJPanel() {
		setSize(new Dimension(360, 28));
		setPreferredSize(new Dimension(360, 28));
		setBounds(new Rectangle(0, 0, 360, 28));
		setLayout(null);
		
		lblkey = new JLabel("lblKey");
		lblkey.setBounds(2, 6, 150, 16);
		add(lblkey);
		
		txtValue = new JTextField();
		txtValue.setBounds(160, 0, 200, 28);
		add(txtValue);
		txtValue.setColumns(10);

	}
	public void setProperty(String key, String value) {
		lblkey.setText(key);
		txtValue.setText(value);
	}
	public String getKey() {
		return lblkey.getText();
	}
	public String getValue() {
		return txtValue.getText();
	}
}
