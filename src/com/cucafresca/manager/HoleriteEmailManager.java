package com.cucafresca.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cucafresca.app.CucaFrescaMailMainApp;
import com.cucafresca.util.FileUtil;
import com.cucafresca.util.MailUtil;
import com.cucafrescal.on.Holerite;

public class HoleriteEmailManager {
	private static Properties EMAIL_PROPERTIES;
	public static String HOLERITE_DIR;
	private static String HOLERITE_SENT;
	
	public HoleriteEmailManager() throws Exception {
		getEmailProperties();
	}
	public static Properties getEmailProperties(String keyEmpresa) {
		Properties propOut = new Properties();
		try {
			for (Object key : getEmailProperties().keySet()) {
				if (key.toString().indexOf(keyEmpresa) != -1) {
					propOut.put(key, getEmailProperties().get(key));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return propOut;
	}
	public static Properties getEmailProperties() throws Exception {
		
		if (EMAIL_PROPERTIES == null) {
			String emailPropertiesFile = CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("holerite.conf.email");
			Logger.getGlobal().info("--> Reading email properties: " + emailPropertiesFile);
			EMAIL_PROPERTIES = new Properties();
			EMAIL_PROPERTIES.load(new FileInputStream(new File(emailPropertiesFile)));
			HOLERITE_DIR = CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("pasta.paraEnviar");
			HOLERITE_SENT = CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("pasta.enviado");
			
			Logger.getGlobal().info("<-- Email properties ready...");
		}
		return EMAIL_PROPERTIES;
	}
	
	public void sendHoleriteFiles() throws Exception {

		
		Logger.getGlobal().info("Reading dir: " + HOLERITE_DIR);
		
		for (File file : new File(HOLERITE_DIR).listFiles()) {
			if (file.isDirectory()) {
				// Obtendo HTML
				File[] fileToSendList = file.listFiles(new FilenameFilter() {
				    public boolean accept(File dir, String name) {
				        return name.toLowerCase().endsWith(".html");
				    }
				});

				
				for (File fileToSend : fileToSendList) {
					Logger.getGlobal().info("Reading File: " + fileToSend.getName());
					String emailKeyHelper[] = fileToSend.getName().replaceAll("\\.", "_").split("_");
					if (emailKeyHelper.length != 4) {
						Logger.getGlobal().log(Level.FINE, "Invalid file " + fileToSend);
						continue;
					}
					String emailKey = emailKeyHelper[1] + "_" + emailKeyHelper[2];
					String email = getEmailProperties().getProperty(emailKey);
					Logger.getGlobal().info("File: " + fileToSend + "\n\tEmail key: " + emailKey + "\n\tEmail: " + email);
					if (email == null) {
						Logger.getGlobal().warning("Invalid e-mail for key " + emailKey);
						System.out.println("Não existe email cadastrado para chave \"" + emailKey + "\" no arquivo \"" + CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("holerite.conf.email") +  "\"");
						continue;
					}
					
					sendHoleriteFile(email, fileToSend);
					
				}
			}
			FileUtil.deleteEmptyDir(file);

		}
		

	}
	public static boolean sendHolerite(Holerite holerite) throws Exception {
		return HoleriteEmailManager.sendHoleriteFile(holerite.getEmail(), new File(holerite.getFileName()));
	}

	public static boolean sendHoleriteFile(String email, File fileToSend) throws Exception {

		
		boolean enviouEmail = MailUtil.send(email, fileToSend);
		if (enviouEmail) {
			File fileDest = new File(fileToSend.getAbsolutePath().replaceAll(HOLERITE_DIR, HOLERITE_SENT));
			FileUtil.createFile(fileDest);
			Logger.getGlobal().info("Moving file :" + fileToSend.toURI() + " to " + fileDest.toURI());
		    Path b1 = Files.move(fileToSend.toPath(),  fileDest.toPath(), StandardCopyOption.ATOMIC_MOVE ,StandardCopyOption.REPLACE_EXISTING );
		    
		    Logger.getGlobal().info("File moved: " + b1);
		}
		
		return enviouEmail;
	}
	public static void main(String[] args) throws Exception {
		Logger.getGlobal().setLevel(Level.ALL);
		CucaFrescaMailMainApp.loadProperties();
		new HoleriteEmailManager().sendHoleriteFiles();
	}
	public static void saveEmailPropertie(Properties propToSave) throws Exception {
		Enumeration<Object> enumPropToSaveKey = propToSave.keys();
		while (enumPropToSaveKey.hasMoreElements()) {
			String keyPropToSave = enumPropToSaveKey.nextElement().toString();
			String valuePropToSave = propToSave.getProperty(keyPropToSave);
			getEmailProperties().put(keyPropToSave, valuePropToSave);
		}
		
		String output = CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("holerite.conf.email");
		getEmailProperties().store(new FileOutputStream(output), "Arquivo de emails");
		Logger.getGlobal().info("Properties saved: " + output);
	}
}
