package com.cucafresca.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.cucafresca.app.CucaFrescaMailMainApp;
import com.cucafresca.util.DataPrinter;
import com.cucafresca.util.FileUtil;
import com.cucafrescal.on.Cabecalho;
import com.cucafrescal.on.Holerite;
import com.cucafrescal.on.CucaFrescaLineDefault;

public class HoleriteManager {
    VelocityEngine ve;
    File file;
    
    public HoleriteManager() {
    	ve = new VelocityEngine();
    	
		file = new File(CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("holerite.template"));		
		Properties p = new Properties();
		p.put("file.resource.loader.path", file.getParent());
		p.put("input.encoding", "UTF-8");
		p.put("output.encoding", "UTF-8");
		try {
			ve.init(p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean validadeFile (File file)  {
		boolean success = true;		
		try {

			// Open the file
			FileInputStream fstream = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			String strLine;

			//Read File Line By Line
			while ((strLine = br.readLine()) != null)   {
				int number = Integer.parseInt(strLine.substring(0, 1));
				if (number < 0 || number > 5) {
					br.close();
					return false;	
				}
			}

			//Close the input stream
			br.close();			
		}catch (Exception e) {
			success = false;
		}

		return success;
		
	}

	public List<Holerite> readHolerite(File file) throws Exception {
		List<Holerite> holeriteList = new LinkedList<Holerite>();
		
		// Open the file
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "ISO8859-1"));

		Holerite holerite = null;
		String line = br.readLine();
		
		do {
			int identificador = Integer.parseInt(line.substring(0, 1));
			CucaFrescaLineDefault lineDefault = CucaFrescaLineDefault.createLineParser(line);
			if (identificador == Cabecalho.IDENTIFICADOR) {
				holerite = new Holerite((Cabecalho) lineDefault);
				holerite.setFileName(file.getAbsolutePath());
				updateEmail(holerite);
				
				holeriteList.add(holerite);
			} else {
				holerite.addLineDefault(lineDefault);
			}
		} while ((line = br.readLine()) != null);


		//Close the input stream
		br.close();
		
		return holeriteList;

	}
	public static void updateEmail(Holerite holerite) throws Exception {
		String emailKey =  holerite.getEmailKey();
		String email = HoleriteEmailManager.getEmailProperties().getProperty(emailKey);
		if (email != null && !email.isEmpty()) {
			holerite.setEmail(email);
		} else {
			HoleriteEmailManager.getEmailProperties().put(emailKey, "");
		}		
	}

	public File saveHolerite(Holerite holerite, String folderNameOut) throws Exception {

		String fileNameOut = 
				CucaFrescaMailMainApp.APP_PROPERTIES.getProperty("pasta.paraEnviar") + "/" + 
				folderNameOut + "/" + 
				folderNameOut.substring(0, folderNameOut.indexOf(".")) + "_" +
				holerite.getCabecalho().getCodigoFuncionario() + ".html";
		
		Logger.getGlobal().log(Level.FINE, "Reading holerite template: " + file.getAbsolutePath());

			
			
	        
        Template template = ve.getTemplate(file.getName());
	        
        VelocityContext context = new VelocityContext();
        
        		
        context.put("holerite", holerite);
        context.put("dataPrinter", new DataPrinter());
        
        

        StringWriter writer = new StringWriter();
        template.merge( context, writer );
        String fileContent = writer.toString();
        
        holerite.setHtml(fileContent);
        
        File htmlfile = new File(fileNameOut);
        Logger.getGlobal().info("Creating file: " + fileNameOut);
        
        FileUtil.createFile(htmlfile);
        
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(htmlfile), "UTF-8"));
		holerite.setFileName(htmlfile.getAbsolutePath());
		
		out.write(fileContent);
		out.close();
		return htmlfile;
		
	}


}
