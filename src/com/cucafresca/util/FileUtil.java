package com.cucafresca.util;

import java.io.File;

public class FileUtil {
	public static void createFile(File file) {
		File parent = file.getParentFile();
        if(!parent.exists() && !parent.mkdirs()){
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }
	}

	public static void deleteEmptyDir(File file) {
		if (file.isDirectory() && file.list().length ==0 ) {
			file.delete();
		}
	}
}
