package com.cucafresca.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang.WordUtils;

public class DataPrinter {
    NumberFormat moneyFormat = new DecimalFormat ("#,##0.00", new DecimalFormatSymbols(new Locale ("pt", "BR")));
    NumberFormat numberFormat = NumberFormat.getInstance(Locale.forLanguageTag("pt-BR"));

	public String printMoney(double number) {
		if (number == 0)
			return "";
		return moneyFormat.format(number);
	}
	public String printNumber(double number) {
		if (number == 0)
			return "";
		return numberFormat.format(number);
	}
	public String printText(String value) {
		if (value == null || value.trim().equals(""))
			return "";
		return WordUtils.capitalize(value.toLowerCase());
	}
	public String printText(String value, String defaultValue) {
		if (value == null || value.trim().equals(""))
			return defaultValue;
		return WordUtils.capitalize(value.toLowerCase());
	}
}
