package com.cucafresca.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.cucafresca.app.CucaFrescaMailMainApp;

public class MailUtil {
	public static Properties MAIL_PROPERTIES;
	public static Transport TRANSPORT;
	
	public static boolean send(String emailAddress, File file) throws Exception {
		byte[] encoded = Files.readAllBytes(file.toPath());
		String html = new String(encoded, MAIL_PROPERTIES.getProperty("mail.iso"));
		return send(emailAddress, html);
		
	}
	public static boolean send(String emailAddress, String html) throws Exception {

	    Properties props = System.getProperties();
	    props.put("mail.smtp.starttls.enable", true); 
	    props.put("mail.smtp.host", MAIL_PROPERTIES.getProperty("mail.smtp"));
	    props.put("mail.smtp.port", MAIL_PROPERTIES.getProperty("mail.port"));
	    props.put("mail.smtp.auth", true);

	    Session session = Session.getInstance(props,null);
	    MimeMessage message = new MimeMessage(session);
	    
	    // Create the email addresses involved
	    try {
	        String subject = html.substring(html.indexOf("<title>")+7, html.indexOf("</title>"));
	        message.setSubject(subject, MAIL_PROPERTIES.getProperty("mail.iso"));
	        InternetAddress fromMail = new InternetAddress(MAIL_PROPERTIES.getProperty("mail.fromName")+" <" + MAIL_PROPERTIES.getProperty("mail.fromMail") +">");
	        message.setFrom(fromMail);
	        message.addRecipient(Message.RecipientType.BCC, fromMail);
	        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddress));
	        
	        // Create a multi-part to combine the parts
	        Multipart multipart = new MimeMultipart("alternative");	

	        // Create your text message part
	        BodyPart messageBodyPart = new MimeBodyPart();
	        // messageBodyPart.setText(text);

	        // Add the text part to the multipart
	        // multipart.addBodyPart(messageBodyPart);

	        // Create the html part
	        messageBodyPart.setContent(html, "text/html; charset=" + MAIL_PROPERTIES.getProperty("mail.iso")); 

	        // Add html part to multi part
	        multipart.addBodyPart(messageBodyPart);

	        // Associate multi-part with message
	        message.setContent(multipart);

	        // Send message
	        Transport transport = getTransport(session);
	        
	        if (transport.isConnected()) {
		        Logger.getGlobal().info("--> Sending message to " + emailAddress);
		        transport.sendMessage(message, message.getAllRecipients());
		        Logger.getGlobal().info("<-- Message Sent");
	        }

	        return true;
	    } catch (Exception e) {
	    	Logger.getGlobal().info("<-- Erro ao enviar email para " + emailAddress);
	    	Logger.getGlobal().severe(e.getMessage());
	    	throw e;
	    } 	
	}
	public static Transport getTransport(Session session) throws Exception {
		if (TRANSPORT == null) {
			TRANSPORT = session.getTransport("smtp");
			Logger.getGlobal().info("Creating SMTP session");
		}
		if (!TRANSPORT.isConnected()) {
			String user = MAIL_PROPERTIES.getProperty("mail.user");
			Logger.getGlobal().info("Connecting to SMTP over user " + user);
			TRANSPORT.connect(user, MAIL_PROPERTIES.getProperty("mail.passwd"));
		}
		return TRANSPORT;
	}
	public static void main(String[] args) throws Exception{
		CucaFrescaMailMainApp.loadProperties();
		String file = "./paraEnviar/01_0001.mes/01_0001_16.html";
		byte[] encoded = Files.readAllBytes(Paths.get(file));
		String html = new String(encoded, MAIL_PROPERTIES.getProperty("mail.iso"));
		
		send("mail@mail.colm.br", html);
		
		
	}
}
