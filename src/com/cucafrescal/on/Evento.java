package com.cucafrescal.on;

import java.util.LinkedList;

public class Evento extends CucaFrescaLineDefault {

	public final static int IDENTIFICADOR = 1;
	public int codigoEvento;
	public String descricaoEvento;
	public String tipo;
	public double vencimentoValor;
	public double quantidade;
	public String referencia;
	
	public Evento(String line) {
		super(line);
	}

	public double getProvento() {
		if (tipo.equals("P")) {
			return vencimentoValor;
		}
		return 0;
	}
	public double getDesconto() {
		if (tipo.equals("D")) {
			return vencimentoValor;
		}
		return 0;
	}
	@Override
	protected LinkedList<String> createLayoutList() {
		LinkedList<String> layoutList = new LinkedList<String>();
		layoutList.add("identificador,1,N");
		layoutList.add("codigoEvento,4,N");
		layoutList.add("descricaoEvento,20,C");
		layoutList.add("tipo,1,C");//P Provento, D Desconto
		layoutList.add("vencimentoValor,10,D");
		layoutList.add("quantidade,8,D");
		layoutList.add("referencia,25,C");
		return layoutList;	
	}


	public int getCodigoEvento() {
		return codigoEvento;
	}

	public void setCodigoEvento(int codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public String getDescricaoEvento() {
		return descricaoEvento;
	}

	public void setDescricaoEvento(String descricaoEvento) {
		this.descricaoEvento = descricaoEvento;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getVencimentoValor() {
		return vencimentoValor;
	}

	public void setVencimentoValor(double vencimentoValor) {
		this.vencimentoValor = vencimentoValor;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}

	public String getReferencia() {
		if (referencia == null) {
			return "";
		}
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

}
