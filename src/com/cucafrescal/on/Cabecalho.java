package com.cucafrescal.on;

import java.util.LinkedList;

public class Cabecalho extends CucaFrescaLineDefault {
	
	public final static int IDENTIFICADOR = 0;
	String nomeEmpresa;
	String nomeEsocial;
	String documentoEmpresa;
	String enderecoEmpresa;
	String mes;
	String ano;
	String nomeFuncionario;
	String descricaoCargo;
	int carteiraTrabalho;
	String nomeBanco;
	String codigoAgencia;
	String contaPagamento;
	double salarioBase;
	String documentoLocal;
	String nomeLocal;
	int codigoFuncionario;
	int codigoApontamento;

	public Cabecalho(String line) {
		super(line);
	}

	public LinkedList<String> createLayoutList() {
		LinkedList<String> layoutList = new LinkedList<String>();
		layoutList.add("identificador,1,N");
		layoutList.add("nomeEmpresa,40,C");

		// 2016-10-20 -> Arquivo AOB não tem o nomeEsocial
		//layoutList.add("nomeEsocial,75,C");
		layoutList.add("documentoEmpresa,16,C");
		layoutList.add("enderecoEmpresa,50,C");
		layoutList.add("mes,10,C");
		layoutList.add("ano,4,C");
		layoutList.add("nomeFuncionario,40,C");
		layoutList.add("descricaoCargo,40,C");
		layoutList.add("carteiraTrabalho,14,N");
		layoutList.add("nomeBanco,3,C");
		layoutList.add("codigoAgencia,4,C");
		layoutList.add("contaPagamento,12,C");
		layoutList.add("salarioBase,10,D");
		layoutList.add("documentoLocal,20,C");
		layoutList.add("nomeLocal,30,C");
		layoutList.add("codigoFuncionario,4,N");
		layoutList.add("codigoApontamento,22,N");
		
		return layoutList;
	}


	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getDocumentoEmpresa() {
		return documentoEmpresa;
	}

	public void setDocumentoEmpresa(String documentoEmpresa) {
		this.documentoEmpresa = documentoEmpresa;
	}

	public String getEnderecoEmpresa() {
		return enderecoEmpresa;
	}

	public void setEnderecoEmpresa(String enderecoEmpresa) {
		this.enderecoEmpresa = enderecoEmpresa;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public String getDescricaoCargo() {
		return descricaoCargo;
	}

	public void setDescricaoCargo(String descricaoCargo) {
		this.descricaoCargo = descricaoCargo;
	}

	public int getCarteiraTrabalho() {
		return carteiraTrabalho;
	}

	public void setCarteiraTrabalho(int carteiraTrabalho) {
		this.carteiraTrabalho = carteiraTrabalho;
	}

	public String getNomeBanco() {
		return nomeBanco;
	}

	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	public String getCodigoAgencia() {
		return codigoAgencia;
	}

	public void setCodigoAgencia(String codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}

	public String getContaPagamento() {
		return contaPagamento;
	}

	public void setContaPagamento(String contaPagamento) {
		this.contaPagamento = contaPagamento;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public String getDocumentoLocal() {
		return documentoLocal;
	}

	public void setDocumentoLocal(String documentoLocal) {
		this.documentoLocal = documentoLocal;
	}

	public String getNomeLocal() {
		return nomeLocal;
	}

	public void setNomeLocal(String nomeLocal) {
		this.nomeLocal = nomeLocal;
	}

	public int getCodigoFuncionario() {
		return codigoFuncionario;
	}

	public void setCodigoFuncionario(int codigoFuncionario) {
		this.codigoFuncionario = codigoFuncionario;
	}

	public int getCodigoApontamento() {
		return codigoApontamento;
	}

	public void setCodigoApontamento(int codigoApontamento) {
		this.codigoApontamento = codigoApontamento;
	}
	



}
