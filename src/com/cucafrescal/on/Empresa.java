package com.cucafrescal.on;

public class Empresa {
	public String codigo;
	public String nome;
	public String fileName;
	public Empresa() {
		
	}
	public Empresa(String fileName) {
		this.fileName = fileName;
		this.codigo = fileName.replaceAll("\\.", "_").split("_")[1];
	}
	@Override
	public String toString() {
		return fileName;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
