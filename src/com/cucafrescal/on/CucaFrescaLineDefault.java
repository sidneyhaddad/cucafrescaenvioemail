package com.cucafrescal.on;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.logging.Logger;

import com.cucafresca.app.CucaFrescaMailMainApp;

public abstract class CucaFrescaLineDefault {
	private LinkedList<String> layoutList = null;

	public int identificador = -1;
	
	public CucaFrescaLineDefault(String line) {
		this.parseLine(line);
	}
	
	protected abstract LinkedList<String> createLayoutList();
	
	private LinkedList<String> getLayoutList() {
		if (layoutList == null) {
			this.layoutList = createLayoutList();
		}
		return this.layoutList;
	}
	private boolean shouldIgnoreLine(String line) {
		int identificador = Integer.parseInt(line.substring(0, 1));
		if (identificador == 3 && line.length() < 5) {
			return true;
		}
		return false;
	}
	private void parseLine(String line) {
		// Validating Line
		if (shouldIgnoreLine(line)) {
			return;
		}
		String keySplit[] = null;
		String fieldName = null;
		String fieldType = null;
		String fieldValue = null;
		int index = 0;
		int lenght = 0;
		try {
			for (String key : this.getLayoutList()) {
				keySplit = key.split(",");
				fieldName = keySplit[0];
				lenght = Integer.parseInt(keySplit[1]);
				fieldType = keySplit[2];
				fieldValue = line.substring(index, index+lenght);
				this.set(fieldName, fieldValue, fieldType);
				if (CucaFrescaMailMainApp.DEBUG) {
					Logger.getGlobal().finest("\t\t" + fieldName + ": [" + fieldValue + "]");
				}
				index = index+lenght;
			}
		}catch (Exception e) {
			System.out.println("Erro ao ler os campos do arquivo");
			System.out.println("keySplit: " + keySplit);
			System.out.println("fieldName: " + fieldName);
			System.out.println("fieldValue: " + fieldValue);
			System.out.println("fieldValue: " + fieldValue);
			System.out.println("index: " + index);
			System.out.println("lenght: " + lenght);
			
			throw e;
		}
		
		
	}
	
	private void set(String fieldName, String fieldValue, String fieldType) {
        try {
        	if (fieldValue.trim().equals(""))
        		return;
            Field field = null;
            if (fieldName.equals("identificador")){
            	field = CucaFrescaLineDefault.class.getDeclaredField(fieldName);
            } else {
            	field = getClass().getDeclaredField(fieldName);
            }
            field.setAccessible(true);
            switch (fieldType) {
			case "C":
	            field.set(this, fieldValue);
				break;
			case "N":
	            field.setInt(this, Integer.parseInt(fieldValue.trim()));
				break;
			case "D":
	            field.setDouble(this, Double.parseDouble(fieldValue.replaceAll(",", ".").trim()));
				break;
			default:
	            field.set(this, fieldValue);
				break;
			}

        } catch (NoSuchFieldException e) {
        	throw new IllegalStateException(e);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }  
	}
	public int getIdentificador() {
		return identificador;
	}
	
	public static CucaFrescaLineDefault createLineParser(String line) {
		int identificador = Integer.parseInt(line.substring(0, 1));
		CucaFrescaLineDefault lineOut = null;
		switch (identificador) {
			case Cabecalho.IDENTIFICADOR:
				lineOut = new Cabecalho(line);
				break;
			case Endereco.IDENTIFICADOR:
				lineOut = new Endereco(line);
				break;
			case Evento.IDENTIFICADOR:
				lineOut = new Evento(line);
				break;
			case Mensagem.IDENTIFICADOR:
				lineOut = new Mensagem(line);
				break;
			case Total.IDENTIFICADOR:
				lineOut = new Total(line);
				break;
			default:
				break;
		}
		return lineOut;
	}

}
