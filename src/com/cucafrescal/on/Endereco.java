package com.cucafrescal.on;

import java.util.LinkedList;

public class Endereco extends CucaFrescaLineDefault{

	public final static int IDENTIFICADOR = 4;
	public String logradouro;
	public String bairro;
	public String cep;
	public String cidade;
	public String uf;
	
	public Endereco(String line) {
		super(line);
	}
	
	@Override
	protected LinkedList<String> createLayoutList() {
		LinkedList<String> layoutList = new LinkedList<String>();
		layoutList.add("identificador,1,N");
		layoutList.add("logradouro,50,C");
		layoutList.add("bairro,40,C");
		layoutList.add("cep,9,C");
		layoutList.add("cidade,8,C");
		//layoutList.add("uf,2,C");
		return layoutList;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	

	
	
}
