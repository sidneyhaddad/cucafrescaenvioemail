package com.cucafrescal.on;

import java.util.LinkedList;

public class Mensagem extends CucaFrescaLineDefault {
	public final static int IDENTIFICADOR = 3;
	public String mensagem1;
	public String mensagem2;
	public String mensagem3;
	

	public Mensagem(String line) {
		super(line);
	}

	@Override
	protected LinkedList<String> createLayoutList() {
		LinkedList<String> layoutList = new LinkedList<String>();
		layoutList.add("identificador,1,N");
		layoutList.add("mensagem1,70,C");
		layoutList.add("mensagem2,70,C");
		layoutList.add("mensagem3,70,C");
		return layoutList;	
	}

}
