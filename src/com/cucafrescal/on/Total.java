package com.cucafrescal.on;

import java.util.LinkedList;

public class Total extends CucaFrescaLineDefault {
	public final static int IDENTIFICADOR = 2;
	public double totalVencimento;
	public double totalDesconto;
	public double liquido;
	public double salarioContribuicaoInss;
	public double baseFgts;
	public double valorFgts;
	public double baseCalculoIrrf;
	
	public Total(String line) {
		super(line);
	}

	@Override
	protected LinkedList<String> createLayoutList() {
		LinkedList<String> layoutList = new LinkedList<String>();
		layoutList.add("identificador,1,N");
		layoutList.add("totalVencimento,10,D");
		layoutList.add("totalDesconto,10,D");
		layoutList.add("liquido,10,D");
		layoutList.add("salarioContribuicaoInss,10,D");
		layoutList.add("baseFgts,10,D");
		layoutList.add("valorFgts,10,D");
		layoutList.add("baseCalculoIrrf,10,D");
		return layoutList;
	}


	public double getTotalVencimento() {
		return totalVencimento;
	}

	public void setTotalVencimento(double totalVencimento) {
		this.totalVencimento = totalVencimento;
	}

	public double getTotalDesconto() {
		return totalDesconto;
	}

	public void setTotalDesconto(double totalDesconto) {
		this.totalDesconto = totalDesconto;
	}

	public double getLiquido() {
		return liquido;
	}

	public void setLiquido(double liquido) {
		this.liquido = liquido;
	}

	public double getSalarioContribuicaoInss() {
		return salarioContribuicaoInss;
	}

	public void setSalarioContribuicaoInss(double salarioContribuicaoInss) {
		this.salarioContribuicaoInss = salarioContribuicaoInss;
	}

	public double getBaseFgts() {
		return baseFgts;
	}

	public void setBaseFgts(double baseFgts) {
		this.baseFgts = baseFgts;
	}

	public double getValorFgts() {
		return valorFgts;
	}

	public void setValorFgts(double valorFgts) {
		this.valorFgts = valorFgts;
	}

	public double getBaseCalculoIrrf() {
		return baseCalculoIrrf;
	}

	public void setBaseCalculoIrrf(double baseCalculoIrrf) {
		this.baseCalculoIrrf = baseCalculoIrrf;
	}
}
