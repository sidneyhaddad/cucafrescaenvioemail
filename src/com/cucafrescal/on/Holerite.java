package com.cucafrescal.on;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Holerite {
	private Cabecalho cabecalho;
	private List<Evento> eventoList;
	private Total total;
	private Mensagem mensagem;
	private Endereco endereco;
	private String html;
	private String fileName;
	private String email;
	private Boolean enviado;
	
	public Holerite(Cabecalho cabecalho) {
		this.cabecalho = cabecalho;
	}
	@Override
	public String toString() {
		return cabecalho.codigoFuncionario + " - " +cabecalho.nomeFuncionario.toUpperCase();
	}
	public String getCodigoEmpresa() {
		String codigoEmpresa[] = fileName.replaceAll("\\.", "_").split("_");
		if (codigoEmpresa.length != 3) {
			Logger.getGlobal().log(Level.FINE, "Invalid file " + fileName);
		}
		return codigoEmpresa[1];
	}
	public void addLineDefault(CucaFrescaLineDefault lineDefault) {
		switch (lineDefault.getIdentificador()) {
		case Cabecalho.IDENTIFICADOR:
			cabecalho = (Cabecalho) lineDefault;
			break;
		case Endereco.IDENTIFICADOR:
			endereco = (Endereco) lineDefault;
			break;
		case Evento.IDENTIFICADOR:
			getEventoList().add((Evento) lineDefault);
			break;
		case Mensagem.IDENTIFICADOR:
			mensagem = (Mensagem) lineDefault;
			break;
		case Total.IDENTIFICADOR:
			total = (Total) lineDefault;
			break;
		default:
			break;
	}		
	}

	public Cabecalho getCabecalho() {
		return cabecalho;
	}

	public void setCabecalho(Cabecalho cabecalho) {
		this.cabecalho = cabecalho;
	}

	public List<Evento> getEventoList() {
		if (eventoList == null) {
			eventoList = new LinkedList<Evento>();
		}
		return eventoList;
	}

	public void setEventoList(List<Evento> eventoList) {
		this.eventoList = eventoList;
	}

	public Total getTotal() {
		return total;
	}

	public void setTotal(Total total) {
		this.total = total;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean isEnviado() {
		return enviado;
	}
	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}
	public String getEmailKey() {
		return this.getCodigoEmpresa() + "_" + this.getCabecalho().getCodigoFuncionario();
	}
}
